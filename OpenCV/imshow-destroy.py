import cv2

img = cv2.imread("IMG/1.jpg")       # image read
cv2.imshow("Photo", img)            # image show
cv2.waitKey(10000)                  # image frame wait --- time = ms    if 0 then keyboard any key press wait
cv2.destroyAllWindows()             # All windows close
#cv2.destroyWindow("Photo")                 # only one window is closed

