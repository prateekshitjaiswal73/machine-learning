import cv2
import cvzone
import pyautogui
import numpy as np

def sc(x, y, dw, dh):
    s = pyautogui.screenshot(region = (x, y, dw, dh))
    s = np.array(s)
    s = cv2.cvtColor(s, cv2.COLOR_RGB2BGR)
    return s

def p(img):
    g = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
    _, b = cv2.threshold(g, 127, 255, cv2.THRESH_BINARY_INV)
    c = cv2.Canny(b, 50, 50)
    k = np.ones((5, 5))
    df = cv2.dilate(c, k, iterations = 2)

    return df

def fo(img, imgP):
    imgC, cF = cvzone.findContours(img, imgP, minArea = 100, filter = None)
    return imgC, cF


def glR(cF, imgC, d = 50):
    if cF:
        lmC = sorted(cF, key = lambda x: x["bbox"][0])
        if (lmC[0]["bbox"][0] <= d) and (lmC[0]["bbox"][0] != 0):
            pyautogui.press("right")


def glL(cF, imgC, d = 170):
    if cF:
        lmC = sorted(cF, key = lambda x: x["bbox"][0], reverse=True)
        if lmC[0]["bbox"][0] >= d:
            pyautogui.press("left")


while True:
    imgL = sc(490, 410, 250, 50)
    imgR = sc(850, 410, 250, 50)

    imgPL = p(imgL)
    imgPR = p(imgR)

    imgCL, cFL = fo(imgL, imgPL)
    imgCR, cFR = fo(imgR, imgPR)

    glL(cFL, imgCL)
    glR(cFR, imgCR)

    cv2.imshow("Left", imgCL)
    cv2.imshow("Right", imgCR)
    cv2.waitKey(1)
