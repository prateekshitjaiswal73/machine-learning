import cv2
from cvzone.HandTrackingModule import HandDetector
import mouse
import numpy as np
import pyautogui


cap = cv2.VideoCapture(0)
camW, camH = 640, 480
cap.set(3, camW)
cap.set(4, camH)
detector = HandDetector(detectionCon = 0.8, maxHands = 2)
frame = 100


while True:
    #print(mouse.get_position())
    success, img = cap.read()
    img = cv2.flip(img, 1)
    hands, img = detector.findHands(img)
    cv2.rectangle(img, (frame, frame), (camW - frame, camH - frame), (255, 0, 255), 2)
    if hands:
        lmlist = hands[0]["lmList"]
        indX, indY = lmlist[8][0], lmlist[8][1]
        cv2.circle(img, (indX, indY), 5, (0, 255, 255), 2)
        convX = int(np.interp(indX, (frame, camW - frame), (0,1919)))
        convY = int(np.interp(indY, (frame, camH - frame), (0,1079)))
        mouse.move(convX, convY)
        fingers = detector.fingersUp(hands[0])
        if fingers[4] == 1:
            pyautogui.mouseDown()

    cv2.imshow("Image", img)
    cv2.waitKey(1)
