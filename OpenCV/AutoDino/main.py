import cv2
import cvzone
import numpy as np
import pyautogui

def captureScreenRegionOpenCV(x, y, desiredWidth, desiredHeight):
    screenshot = pyautogui.screenshot(region=(x, y, desiredWidth, desiredHeight))
    screenshot = np.array(screenshot)
    screenshot = cv2.cvtColor(screenshot, cv2.COLOR_RGB2BGR)
    return screenshot

def preProcess(imgCrop):
    grayFrame = cv2.cvtColor(imgCrop, cv2.COLOR_BGR2GRAY)

    # Convert in white and black
    _, binaryFrame = cv2.threshold(grayFrame, 127, 255, cv2.THRESH_BINARY_INV)

    # Convert edges into white and everything else balck
    cannyFrame = cv2.Canny(binaryFrame, 50, 50)
    kernel = np.ones((5, 5))
    # make the edges proper
    dilatedFrame = cv2.dilate(cannyFrame, kernel, iterations=2)

    return dilatedFrame

def findObstacles(imgCrop, imgPre):
    imgContours, conFound = cvzone.findContours(imgCrop, imgPre, minArea = 100, filter = None) # minArea to reduce noise
    return imgContours, conFound

def gameLogic(conFound, imgContours, jumpDistance = 90):
    if conFound:
        # left most contour
        leftMostContour = sorted(conFound, key = lambda x: x["bbox"][0])

        cv2.line(imgContours, (0, leftMostContour[0]["bbox"][1] + 10),
                 (leftMostContour[0]["bbox"][0], leftMostContour[0]["bbox"][1] + 10), (0, 200, 0), 10)


        if leftMostContour[0]["bbox"][0] < jumpDistance:
            pyautogui.press("space")

    return imgContours


while True:
    # Step 1 - Capture the screen region of game
    imgGame = captureScreenRegionOpenCV(450, 400, 650, 200)

    # Step 2 - Crop the image to the desired region
    cp = 100, 135, 110
    imgCrop = imgGame[cp[0]:cp[1], cp[2]:]

    # Step 3 - Pre Process Image
    imgPre = preProcess(imgCrop)

    # Step 4 - Find Obstacle
    imgContours, conFound = findObstacles(imgCrop, imgPre)

    # Step 5 - Apply Game Logic
    imgContours = gameLogic(conFound, imgContours)

    # Step 6 - Display the result
    imgGame[cp[0]:cp[1], cp[2]:] = imgContours

    #cv2.imshow("Game", imgGame)
    #cv2.imshow("Crop-Game", imgCrop)
    cv2.waitKey(1)
