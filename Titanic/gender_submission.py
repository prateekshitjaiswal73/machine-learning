import pandas as pd
from sklearn.linear_model import LinearRegression
from sklearn.model_selection import train_test_split
import math

import plotext as plt

df = pd.read_csv("gender_submission.csv")
X = df.PassengerId.head()
y = df.Survived
plt.xlabel("Passenger ID")
plt.ylabel("Survived")
plt.scatter(X, y, color="red", marker="+")
plt.show()
#X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.2)
r = LinearRegression()
#r.fit([X_train], y_train)
r.fit(df[["PassengerId"]], df.Survived)
#print(df.head())
p = r.predict([[1306]])
a = round(p[0])
#print(p)
