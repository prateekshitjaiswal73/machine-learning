import pandas as pd
from sklearn.linear_model import LinearRegression
import math
import numpy as np

df = pd.read_csv("test.csv")
df.Age = df.Age.fillna(math.floor(df.Age.median()))
df.Fare = df.Fare.fillna(math.floor(df.Fare.median()))

print(df)

