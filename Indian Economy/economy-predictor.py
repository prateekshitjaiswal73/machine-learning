import pandas as pd
from sklearn.linear_model import LinearRegression
#from sklearn.
import sys

df= pd.read_csv("canada_per_capita_income.csv")
year = df[["year"]]
per = df.drop(columns="year")
r = LinearRegression()
r.fit(year, per)
print(r.predict([[2022]]))
