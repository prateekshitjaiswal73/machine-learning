import sys
import plotext as plt
import pandas as pd
from sklearn.linear_model import LinearRegression
#import matplotlib.pyplot as plt
import numpy as np

df = pd.read_csv("homeprices.csv")

plt.xlabel('area')
plt.ylabel('price')
plt.scatter(df.area, df.price, color="red", marker="+")

#plt.show()


r = LinearRegression()
r.fit(df[['area']], df.price)

plt.xlabel('area')
plt.ylabel('price')
plt.scatter(df.area, df.price, color="red", marker="+")
plt.plot(df.area, r.predict(df[['area']]), color="blue")
#plt.show()

d = pd.read_csv("home.csv")

p = r.predict(d)
d['Prices'] = p
d.to_csv("prediction.csv")
fd = pd.read_csv("prediction.csv")
print(p)
