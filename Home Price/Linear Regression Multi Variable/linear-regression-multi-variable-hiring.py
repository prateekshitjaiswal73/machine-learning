import pandas as pd
import numpy as np
from sklearn.linear_model import LinearRegression
import math
from sklearn.model_selection import train_test_split
from word2number.w2n import word_to_num

df = pd.read_csv("hiring.csv")
df.experience = df.experience.fillna("zero")
df.experience = df.experience.apply(word_to_num)

df[["test_score(out of 10)"]] = df[["test_score(out of 10)"]].fillna(df[["test_score(out of 10)"]].median())

X = df.drop(columns="salary($)")
y = df[["salary($)"]]

r = LinearRegression()
r.fit(X, y)
p = r.predict([[3, 7.0, 10]])
a = round(p[0][0])
print(a)
