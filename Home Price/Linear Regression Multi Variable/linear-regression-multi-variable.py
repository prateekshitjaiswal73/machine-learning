import pandas as pd
import numpy as np
from sklearn.linear_model import LinearRegression
import math
from sklearn.model_selection import train_test_split

df = pd.read_csv("homeprices.csv")
median_bedrooms = math.floor(df.bedrooms.median())

df.bedrooms = df.bedrooms.fillna(median_bedrooms)

X = df[['area', 'bedrooms', 'age']]
y = df.price

r = LinearRegression()
r.fit(X, y)
print(df)
p = [[2500, 4, 20]]
print(p)
print(r.predict(p))
