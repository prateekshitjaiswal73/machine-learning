import pickle
import os
import pandas as pd
from sklearn.ensemble import StackingClassifier
from sklearn.linear_model import LogisticRegression
from sklearn.model_selection import train_test_split
from sklearn.metrics import accuracy_score, confusion_matrix, roc_curve, auc, precision_recall_curve, precision_score, recall_score, f1_score
import json
import matplotlib.pyplot as plt
import seaborn as sns

# Load the dataset (make sure to use the same preprocessing as in training)
data = pd.read_csv('heart.csv')
X = data.drop('target', axis=1)
y = data['target']

# Create a new folder to save plots
output_dir = 'Plots'
if not os.path.exists(output_dir):
    os.makedirs(output_dir)

if not os.path.exists('Model_Learning'):
    os.makedirs('Model_Learning')

# Split the dataset into training and testing sets
X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.3, random_state=42)

# Split the dataset into train and test sets for SVM
X_train_svm, X_test_svm, y_train_svm, y_test_svm = train_test_split(X, y, test_size=0.25, random_state=32)

# Load the trained models
xgb_model = pickle.load(open('Model_Learning/xgb_clf_model.sav', 'rb'))
svm_model = pickle.load(open('Model_Learning/svm_clf_model.sav', 'rb'))
dt_model = pickle.load(open('Model_Learning/decisionTree_clf_model.sav', 'rb'))

# Define base models for stacking
base_models = [
    ('xgb', xgb_model),
    ('svm', svm_model),
    ('dt', dt_model)
]

# Define the meta-learner
meta_learner = LogisticRegression()

# Create the stacking classifier
stacking_model = StackingClassifier(estimators=base_models, final_estimator=meta_learner)

# Train the stacking model
stacking_model.fit(X_train, y_train)

# Make predictions and evaluate the model
y_pred = stacking_model.predict(X_test)
accuracy = accuracy_score(y_test, y_pred)
dt_precision = precision_score(y_test, y_pred)
dt_recall = recall_score(y_test, y_pred)
dt_f1 = f1_score(y_test, y_pred)  # F1-score

print(f"Accuracy for Hybrid Model: {accuracy * 100.0}%")
print(f"Precision for Hybrid Model: {dt_precision * 100.0}%")
print(f"Recall for Hybrid Model: {dt_recall * 100.0}%")
print(f"F1-Score for Hybrid Model: {dt_f1 * 100.0}%")

# Save model
pickle.dump(stacking_model, open('Model_Learning/hybrid_model.sav', 'wb'))

# Update model_results.json with hybrid model results
# Load the existing model results
with open('model_results.json', 'r') as f:
    model_performance = json.load(f)

# Add hybrid model results
model_performance['Hybrid Model'] = {
    'Accuracy': accuracy * 100.0,
    'Precision': dt_precision * 100.0,
    'Recall': dt_recall * 100.0,
    'F1 Score': dt_f1 * 100.0
}

# Save the updated results back to the JSON file
with open('model_results.json', 'w') as f:
    json.dump(model_performance, f, indent=4)


# Visualization Functions
def save_plot(fig, filename):
    path = os.path.join(output_dir, filename)
    fig.savefig(path)
    plt.close(fig)


def plot_confusion_matrix(y_test, y_pred, model_name):
    cm = confusion_matrix(y_test, y_pred)
    fig, ax = plt.subplots()
    sns.heatmap(cm, annot=True, fmt='d', ax=ax)
    ax.set_title(f'Confusion Matrix for {model_name}')
    ax.set_ylabel('Actual label')
    ax.set_xlabel('Predicted label')
    save_plot(fig, f'confusion_matrix_{model_name}.png')


def plot_roc_curve(models_metrics, model_names):
    fig, ax = plt.subplots()
    for metrics, name in zip(models_metrics, model_names):
        fpr, tpr, _ = roc_curve(metrics['y_test'], metrics['y_pred_prob'])
        roc_auc = auc(fpr, tpr)
        ax.plot(fpr, tpr, lw=2, label=f'{name} (AUC = {roc_auc:.2f})')

    ax.plot([0, 1], [0, 1], color='navy', lw=2, linestyle='--')
    ax.set_xlim([0.0, 1.0])
    ax.set_ylim([0.0, 1.05])
    ax.set_xlabel('False Positive Rate')
    ax.set_ylabel('True Positive Rate')
    ax.set_title('Receiver Operating Characteristic')
    ax.legend(loc="lower right")
    save_plot(fig, 'roc_curve.png')


def plot_precision_recall_curve(models_metrics, model_names):
    fig, ax = plt.subplots()
    for metrics, name in zip(models_metrics, model_names):
        precision, recall, _ = precision_recall_curve(metrics['y_test'], metrics['y_pred_prob'])
        ax.plot(recall, precision, lw=2, label=f'{name}')

    ax.set_xlabel('Recall')
    ax.set_ylabel('Precision')
    ax.set_title('Precision-Recall Curve')
    ax.legend(loc="lower left")
    save_plot(fig, 'precision_recall_curve.png')


def plot_feature_importance(model, model_name):
    # Check if the model has feature_importances_ attribute
    if hasattr(model, 'feature_importances_'):
        importance = model.feature_importances_
        features = X.columns  # Make sure X is defined and has the same columns used for training
        importance_df = pd.DataFrame({'Feature': features, 'Importance': importance})
        importance_df = importance_df.sort_values(by='Importance', ascending=False)

        # Plotting the feature importance
        fig, ax = plt.subplots(figsize=(10, 6))
        sns.barplot(x='Importance', y='Feature', data=importance_df, ax=ax)
        ax.set_title(f'Feature Importance for {model_name}')

        # Save the plot if save_plot function exists
        if 'save_plot' in globals():
            save_plot(fig, f'feature_importance_{model_name}.png')
        else:
            plt.show()  # Show the plot if save_plot isn't defined
    else:
        print(f"{model_name} does not have feature_importances_ attribute.")


y_pred_xgb = xgb_model.predict(X_test)
y_pred_svm = svm_model.predict(X_test_svm)
y_pred_dt = dt_model.predict(X_test)

# Metrics for ROC and Precision-Recall curves
xgb_metrics = {'y_pred_prob': xgb_model.predict_proba(X_test)[:, 1], 'y_test': y_test, 'y_pred': y_pred_xgb}
svm_metrics = {'y_pred_prob': svm_model.predict_proba(X_test_svm)[:, 1], 'y_test': y_test_svm, 'y_pred': y_pred_svm}
dt_metrics = {'y_pred_prob': dt_model.predict_proba(X_test)[:, 1], 'y_test': y_test, 'y_pred': y_pred_dt}
stacking_metrics = {'y_pred_prob': stacking_model.predict_proba(X_test)[:, 1], 'y_test': y_test, 'y_pred': y_pred}

# Plot confusion matrices
plot_confusion_matrix(stacking_metrics['y_test'], stacking_metrics['y_pred'], 'Hybrid Model')

# Plot ROC curves
plot_roc_curve([stacking_metrics, xgb_metrics, svm_metrics, dt_metrics], ['Hybrid Model', 'XGBoost', 'SVM', 'Decision Tree'])

# Plot Precision-Recall curves
plot_precision_recall_curve([stacking_metrics, xgb_metrics, svm_metrics, dt_metrics], ['Hybrid Model', 'XGBoost', 'SVM', 'Decision Tree'])

# Plot Feature Importance
plot_feature_importance(stacking_model, 'Hybrid Model')
