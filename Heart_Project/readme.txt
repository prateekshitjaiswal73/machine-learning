1. Set-ExecutionPolicy Unrestricted -Scope Process
2. Create Venv by 
    python -m venv env
3. .\env\Scripts\Activate.ps1 (Activate env)
4. pip install -r .\requirement.txt (Install dependancies)
5. python traning_model.py
6. streamlit run .\stream_dashboard.py (Execute Frontend)
