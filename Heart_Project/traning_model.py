# Import necessary libraries
from sklearn.model_selection import train_test_split, learning_curve
from sklearn.metrics import accuracy_score, confusion_matrix, roc_curve, auc, precision_recall_curve, precision_score, recall_score, f1_score
from sklearn.preprocessing import LabelEncoder
from xgboost import XGBClassifier, plot_importance
from sklearn.tree import DecisionTreeClassifier
import matplotlib.pyplot as plt
from sklearn.svm import SVC
import seaborn as sns
import pandas as pd
import numpy as np
import pickle
import os
import json

# Load the dataset
data = pd.read_csv('heart.csv')

# Create a new folder to save plots
output_dir = 'Plots'
if not os.path.exists(output_dir):
    os.makedirs(output_dir)

if not os.path.exists('Model_Learning'):
    os.makedirs('Model_Learning')

# Data Preprocessing
print('Total duplicate values -> ',data['target'].value_counts())  # check for Imbalance Data
print('Total duplicate values -> ',data.duplicated())  # check for duplicates
print('null values -> ')
print(data.isnull().sum())  # check for null values
print()
print(data.info())  # check for encoding

# Separate features and target
X = data.drop('target', axis=1)  # all columns except the last one
y = data['target']               # the last column

# Encode categorical variables
categorical_features = X.select_dtypes(include=['object']).columns
for feature in categorical_features:
    X[feature].fillna(X[feature].mode()[0], inplace=True)  # Fill missing values with mode
    le = LabelEncoder()
    X[feature] = le.fit_transform(X[feature])

# Encode target variable
le = LabelEncoder()
y = le.fit_transform(y)

# Split the dataset into train and test sets for XGBoost
X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.3, random_state=42)

# Split the dataset into train and test sets for SVM
X_train_svm, X_test_svm, y_train_svm, y_test_svm = train_test_split(X, y, test_size=0.25, random_state=32)

# XGBoost Model Training with hyperparameters to reduce overfitting
xgb_clf = XGBClassifier(
    objective='binary:logistic',
    eval_metric='logloss',
    learning_rate=0.01,
    n_estimators=600,
    max_depth=4,
    min_child_weight=3,
    subsample=0.7,
    colsample_bytree=0.7,
    reg_alpha=0.01,
    reg_lambda=0.01,
    # gamma=0.1
)

# Fit the model with early stopping to avoid overfitting
xgb_clf.fit(
    X_train, y_train,
    eval_set=[(X_test, y_test)],
    verbose=True
)

# Model Evaluation for XGBoost
y_pred_xgb = xgb_clf.predict(X_test)
xgb_accuracy = accuracy_score(y_test, y_pred_xgb)
xgb_precision = precision_score(y_test, y_pred_xgb)
xgb_recall = recall_score(y_test, y_pred_xgb)
xgb_f1 = f1_score(y_test, y_pred_xgb)  # F1-score

print(f"Accuracy for XGBoost: {xgb_accuracy * 100.0}%")
print(f"Precision for XGBoost: {xgb_precision * 100.0}%")
print(f"Recall for XGBoost: {xgb_recall * 100.0}%")
print(f"F1-Score for XGBoost: {xgb_f1 * 100.0}%")

print('----------------------------------')

# SVM Model Training
svm_clf = SVC(kernel='linear', C=1.0, probability=True, random_state=311)
svm_clf.fit(X_train_svm, y_train_svm)

# Model Evaluation for SVM
y_pred_svm = svm_clf.predict(X_test_svm)
svm_accuracy = accuracy_score(y_test_svm, y_pred_svm)
svm_precision = precision_score(y_test_svm, y_pred_svm)
svm_recall = recall_score(y_test_svm, y_pred_svm)
svm_f1 = f1_score(y_test_svm, y_pred_svm)  # F1-score

print(f"Accuracy for SVM: {svm_accuracy * 100.0}%")
print(f"Precision for SVM: {svm_precision * 100.0}%")
print(f"Recall for SVM: {svm_recall * 100.0}%")
print(f"F1-Score for SVM: {svm_f1 * 100.0}%")

print('----------------------------------')

# Decision Tree Model Training
decisionTree_clf = DecisionTreeClassifier(
    random_state=45,
    max_depth=10,
    ccp_alpha=0.005 # for Regularization our model by pruning the branches
)
decisionTree_clf.fit(X_train, y_train)

# Model Evaluation for Decision Tree
y_pred_dt = decisionTree_clf.predict(X_test)
dt_accuracy = accuracy_score(y_test, y_pred_dt)
dt_precision = precision_score(y_test, y_pred_dt)
dt_recall = recall_score(y_test, y_pred_dt)
dt_f1 = f1_score(y_test, y_pred_dt)  # F1-score

print(f"Accuracy for Decision Tree: {dt_accuracy * 100.0}%")
print(f"Precision for Decision Tree: {dt_precision * 100.0}%")
print(f"Recall for Decision Tree: {dt_recall * 100.0}%")
print(f"F1-Score for Decision Tree: {dt_f1 * 100.0}%")

# Save models
pickle.dump(xgb_clf, open('Model_Learning/xgb_clf_model.sav', 'wb'))
pickle.dump(svm_clf, open('Model_Learning/svm_clf_model.sav', 'wb'))
pickle.dump(decisionTree_clf, open('Model_Learning/decisionTree_clf_model.sav', 'wb'))

# Save evaluation results to a JSON file
results = {
    'XGBoost': {
        'Accuracy': xgb_accuracy * 100.0,
        'Precision': xgb_precision * 100.0,
        'Recall': xgb_recall * 100.0,
        'F1 Score': xgb_f1 * 100.0
    },
    'SVM': {
        'Accuracy': svm_accuracy * 100.0,
        'Precision': svm_precision * 100.0,
        'Recall': svm_recall * 100.0,
        'F1 Score': svm_f1 * 100.0
    },
    'Decision Tree': {
        'Accuracy': dt_accuracy * 100.0,
        'Precision': dt_precision * 100.0,
        'Recall': dt_recall * 100.0,
        'F1 Score': dt_f1 * 100.0
    }
}

# Write the results dictionary to a JSON file
with open('model_results.json', 'w') as json_file:
    json.dump(results, json_file, indent=4)

print("Results saved to 'model_results.json'")

# Visualization Functions
def save_plot(fig, filename):
    path = os.path.join(output_dir, filename)
    fig.savefig(path)
    plt.close(fig)

def plot_confusion_matrix(y_test, y_pred, model_name):
    cm = confusion_matrix(y_test, y_pred)
    fig, ax = plt.subplots()
    sns.heatmap(cm, annot=True, fmt='d', ax=ax)
    ax.set_title(f'Confusion Matrix for {model_name}')
    ax.set_ylabel('Actual label')
    ax.set_xlabel('Predicted label')
    save_plot(fig, f'confusion_matrix_{model_name}.png')

def plot_roc_curve(models_metrics, model_names):
    fig, ax = plt.subplots()
    for metrics, name in zip(models_metrics, model_names):
        fpr, tpr, _ = roc_curve(metrics['y_test'], metrics['y_pred_prob'])
        roc_auc = auc(fpr, tpr)
        ax.plot(fpr, tpr, lw=2, label=f'{name} (AUC = {roc_auc:.2f})')
    
    ax.plot([0, 1], [0, 1], color='navy', lw=2, linestyle='--')
    ax.set_xlim([0.0, 1.0])
    ax.set_ylim([0.0, 1.05])
    ax.set_xlabel('False Positive Rate')
    ax.set_ylabel('True Positive Rate')
    ax.set_title('Receiver Operating Characteristic')
    ax.legend(loc="lower right")
    save_plot(fig, 'roc_curve.png')

def plot_precision_recall_curve(models_metrics, model_names):
    fig, ax = plt.subplots()
    for metrics, name in zip(models_metrics, model_names):
        precision, recall, _ = precision_recall_curve(metrics['y_test'], metrics['y_pred_prob'])
        ax.plot(recall, precision, lw=2, label=f'{name}')
    
    ax.set_xlabel('Recall')
    ax.set_ylabel('Precision')
    ax.set_title('Precision-Recall Curve')
    ax.legend(loc="lower left")
    save_plot(fig, 'precision_recall_curve.png')

def plot_feature_importance(model, model_name):
    # Check if the model has feature_importances_ attribute
    if hasattr(model, 'feature_importances_'):
        importance = model.feature_importances_
        features = X.columns  # Make sure X is defined and has the same columns used for training
        importance_df = pd.DataFrame({'Feature': features, 'Importance': importance})
        importance_df = importance_df.sort_values(by='Importance', ascending=False)
        
        # Plotting the feature importance
        fig, ax = plt.subplots(figsize=(10, 6))
        sns.barplot(x='Importance', y='Feature', data=importance_df, ax=ax)
        ax.set_title(f'Feature Importance for {model_name}')
        
        # Save the plot if save_plot function exists
        if 'save_plot' in globals():
            save_plot(fig, f'feature_importance_{model_name}.png')
        else:
            plt.show()  # Show the plot if save_plot isn't defined
    else:
        print(f"{model_name} does not have feature_importances_ attribute.")

# Metrics for ROC and Precision-Recall curves
xgb_metrics = {'y_pred_prob': xgb_clf.predict_proba(X_test)[:, 1], 'y_test': y_test, 'y_pred': y_pred_xgb}
svm_metrics = {'y_pred_prob': svm_clf.predict_proba(X_test_svm)[:, 1], 'y_test': y_test_svm, 'y_pred': y_pred_svm}
dt_metrics = {'y_pred_prob': decisionTree_clf.predict_proba(X_test)[:, 1], 'y_test': y_test, 'y_pred': y_pred_dt}

# Plot confusion matrices
plot_confusion_matrix(xgb_metrics['y_test'], xgb_metrics['y_pred'], 'XGBoost')
plot_confusion_matrix(svm_metrics['y_test'], svm_metrics['y_pred'], 'SVM')
plot_confusion_matrix(dt_metrics['y_test'], dt_metrics['y_pred'], 'Decision Tree')

# Plot ROC curves
plot_roc_curve([xgb_metrics, svm_metrics, dt_metrics], ['XGBoost', 'SVM', 'Decision Tree'])

# Plot Precision-Recall curves
plot_precision_recall_curve([xgb_metrics, svm_metrics, dt_metrics], ['XGBoost', 'SVM', 'Decision Tree'])

# Plot Feature Importance
plot_feature_importance(xgb_clf, 'XGBoost')
plot_feature_importance(decisionTree_clf, 'Decision Tree')


# # Import necessary libraries
# from sklearn.model_selection import train_test_split, learning_curve
# from sklearn.metrics import accuracy_score, confusion_matrix, roc_curve, auc, precision_recall_curve, precision_score, recall_score
# from sklearn.preprocessing import LabelEncoder
# from xgboost import XGBClassifier, plot_importance
# from sklearn.tree import DecisionTreeClassifier
# import matplotlib.pyplot as plt
# from sklearn.svm import SVC
# import seaborn as sns
# import pandas as pd
# import numpy as np
# import pickle
# import os



# # Load the dataset
# data = pd.read_csv('heart.csv')

# # Create a new folder to save plots
# output_dir = 'Plots'
# if not os.path.exists(output_dir):
#     os.makedirs(output_dir)

# if not os.path.exists('Model_Learning'):
#     os.makedirs('Model_Learning')

# # Data Preprocessing

# print('Total duplicate values -> ',data['target'].value_counts()) # check for Imbalance Data
# print('Total duplicate values -> ',data.duplicated()) # check for duplicates
# print('null values -> ')
# print(data.isnull().sum()) # check for null values
# print()
# print(data.info()) # check for encoding

# # Separate features and target
# X = data.drop('target', axis=1)  # all columns except the last one
# y = data['target']               # the last column

# # Encode categorical variables
# categorical_features = X.select_dtypes(include=['object']).columns
# for feature in categorical_features:
#     X[feature].fillna(X[feature].mode()[0], inplace=True)  # Fill missing values with mode
#     le = LabelEncoder()
#     X[feature] = le.fit_transform(X[feature])

# # Encode target variable
# le = LabelEncoder()
# y = le.fit_transform(y)

# # Split the dataset into train and test sets for xgboost
# X_train_xg, X_test_xg, y_train_xg, y_test_xg = train_test_split(X, y, test_size=0.25, random_state=2)

# # Split the dataset into train and test sets for SVM
# X_train_svm, X_test_svm, y_train_svm, y_test_svm = train_test_split(X, y, test_size=0.25, random_state=32)

# # Split the dataset into train and test sets for Decision Tree  
# X_train_dt, X_test_dt, y_train_dt, y_test_dt = train_test_split(X, y, test_size=0.25, random_state=42)

# # XGboost Model Training
# xgb_clf = XGBClassifier(objective='binary:logistic', eval_metric='logloss')
# xgb_clf.fit(X_train_xg, y_train_xg)

# # Model Evaluation for XGboost
# y_pred_xgb = xgb_clf.predict(X_test_xg)
# xgb_accuracy = accuracy_score(y_test_xg, y_pred_xgb)
# print(f"Accuracy for XGboost: {xgb_accuracy * 100.0}%")

# # XGBoost Model Evaluation (Precision and Recall)
# xgb_precision = precision_score(y_test_xg, y_pred_xgb)
# xgb_recall = recall_score(y_test_xg, y_pred_xgb)
# print(f"Precision for XGBoost: {xgb_precision * 100.0}%")
# print(f"Recall for XGBoost: {xgb_recall * 100.0}%")

# print('----------------------------------')
# # SVM Model Training
# svm_clf = SVC(kernel='linear', C=1.0, probability=True, random_state=311)
# svm_clf.fit(X_train_svm, y_train_svm)

# # Model Evaluation for SVM
# y_pred_svm = svm_clf.predict(X_test_svm)
# svm_accuracy = accuracy_score(y_test_svm, y_pred_svm)
# print(f"Accuracy for SVM : {svm_accuracy * 100.0}%")

# # SVM Model Evaluation (Precision and Recall)
# svm_precision = precision_score(y_test_svm, y_pred_svm)
# svm_recall = recall_score(y_test_svm, y_pred_svm)
# print(f"Precision for SVM: {svm_precision * 100.0}%")
# print(f"Recall for SVM: {svm_recall * 100.0}%")

# print('----------------------------------')

# # Decision Tree Model Training
# decisionTree_clf = DecisionTreeClassifier(random_state=45)
# decisionTree_clf.fit(X_train_dt, y_train_dt)

# # Model Evaluation for Decision Trees
# y_pred_dt = decisionTree_clf.predict(X_test_dt)
# dt_accuracy = accuracy_score(y_test_dt, y_pred_dt)
# print(f"Accuracy for Decision Tree: {dt_accuracy * 100.0}%")

# # Decision Tree Model Evaluation (Precision and Recall)
# dt_precision = precision_score(y_test_dt, y_pred_dt)
# dt_recall = recall_score(y_test_dt, y_pred_dt)
# print(f"Precision for Decision Tree: {dt_precision * 100.0}%")
# print(f"Recall for Decision Tree: {dt_recall * 100.0}%")

# # Save models
# pickle.dump(xgb_clf, open('Model_Learning/xgb_clf_model.sav', 'wb'))
# pickle.dump(svm_clf, open('Model_Learning/svm_clf_model.sav', 'wb'))
# pickle.dump(decisionTree_clf, open('Model_Learning/decisionTree_clf_model.sav', 'wb'))

# # Visualization Functions
# def save_plot(fig, filename):
#     path = os.path.join(output_dir, filename)
#     fig.savefig(path)
#     plt.close(fig)

# def plot_confusion_matrix(y_test, y_pred, model_name):
#     cm = confusion_matrix(y_test, y_pred)
#     fig, ax = plt.subplots()
#     sns.heatmap(cm, annot=True, fmt='d', ax=ax)
#     ax.set_title(f'Confusion Matrix for {model_name}')
#     ax.set_ylabel('Actual label')
#     ax.set_xlabel('Predicted label')
#     save_plot(fig, f'confusion_matrix_{model_name}.png')

# def plot_roc_curve(models_metrics, model_names):
#     fig, ax = plt.subplots()
#     for metrics, name in zip(models_metrics, model_names):
#         fpr, tpr, _ = roc_curve(y_test, metrics['y_pred_prob'])
#         roc_auc = auc(fpr, tpr)
#         ax.plot(fpr, tpr, lw=2, label=f'{name} (AUC = {roc_auc:.2f})')
    
#     ax.plot([0, 1], [0, 1], color='navy', lw=2, linestyle='--')
#     ax.set_xlim([0.0, 1.0])
#     ax.set_ylim([0.0, 1.05])
#     ax.set_xlabel('False Positive Rate')
#     ax.set_ylabel('True Positive Rate')
#     ax.set_title('Receiver Operating Characteristic')
#     ax.legend(loc="lower right")
#     save_plot(fig, 'roc_curve.png')

# def plot_precision_recall_curve(models_metrics, model_names):
#     fig, ax = plt.subplots()
#     for metrics, name in zip(models_metrics, model_names):
#         precision, recall, _ = precision_recall_curve(y_test, metrics['y_pred_prob'])
#         ax.plot(recall, precision, lw=2, label=f'{name}')
    
#     ax.set_xlabel('Recall')
#     ax.set_ylabel('Precision')
#     ax.set_title('Precision-Recall Curve')
#     ax.legend(loc="lower left")
#     save_plot(fig, 'precision_recall_curve.png')

# def plot_feature_importance(model, model_name):
#     if hasattr(model, 'feature_importances_'):
#         importance = model.feature_importances_
#         features = X.columns
#         importance_df = pd.DataFrame({'Feature': features, 'Importance': importance})
#         importance_df = importance_df.sort_values(by='Importance', ascending=False)
        
#         fig, ax = plt.subplots(figsize=(10, 6))
#         sns.barplot(x='Importance', y='Feature', data=importance_df, ax=ax)
#         ax.set_title(f'Feature Importance for {model_name}')
#         save_plot(fig, f'feature_importance_{model_name}.png')

# # Metrics for ROC and Precision-Recall curves
# xgb_metrics = {'y_pred_prob': xgb_clf.predict_proba(X_test)[:, 1], 'y_pred': y_pred_xgb}
# svm_metrics = {'y_pred_prob': svm_clf.predict_proba(X_test)[:, 1], 'y_pred': y_pred_svm}
# dt_metrics = {'y_pred_prob': decisionTree_clf.predict_proba(X_test)[:, 1], 'y_pred': y_pred_dt}

# # Plot confusion matrices
# plot_confusion_matrix(y_test, xgb_metrics['y_pred'], 'XGBoost')
# plot_confusion_matrix(y_test, svm_metrics['y_pred'], 'SVM')
# plot_confusion_matrix(y_test, dt_metrics['y_pred'], 'Decision Tree')

# # Plot ROC curves
# plot_roc_curve([xgb_metrics, svm_metrics, dt_metrics], ['XGBoost', 'SVM', 'Decision Tree'])

# # Plot Precision-Recall curves
# plot_precision_recall_curve([xgb_metrics, svm_metrics, dt_metrics], ['XGBoost', 'SVM', 'Decision Tree'])

# # Plot Feature Importance
# plot_feature_importance(xgb_clf, 'XGBoost')
# plot_feature_importance(decisionTree_clf, 'Decision Tree')
