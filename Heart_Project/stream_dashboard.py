# Import necessary libraries
import numpy as np
import pickle
import streamlit as st
import json

# Loading the saved models
models = {
    'XGBoost': pickle.load(open('Model_Learning/xgb_clf_model.sav', 'rb')),
    'SVM': pickle.load(open('Model_Learning/svm_clf_model.sav', 'rb')),
    'Decision Tree': pickle.load(open('Model_Learning/decisionTree_clf_model.sav', 'rb')),
    'Hybrid Model': pickle.load(open('Model_Learning/hybrid_model.sav', 'rb'))

}


# Load the model performance metrics from a JSON file
with open('model_results.json', 'r') as f:
    model_performance = json.load(f)

# Creating a function for prediction
def predict_heart_disease(model, input_data):
    # Changing the input_data to numpy array
    input_data_as_numpy_array = np.asarray(input_data)
    # Reshape the array as we are predicting for one instance
    input_data_reshaped = input_data_as_numpy_array.reshape(1, -1)
    # Making prediction
    prediction = model.predict(input_data_reshaped)
    
    if prediction[0] == 0:
        return 'The person does not have heart disease'
    else:
        return 'The person has heart disease'

# Function to show model analytics
def show_model_analytics(model_name):
    st.subheader(f'Model Analytics for {model_name}')
    model = models[model_name]
    
    selected_metrics = model_performance[model_name]

    st.subheader(f'Model Performance Metrics: {model_name}')
    st.write(f"Accuracy: {selected_metrics['Accuracy']:.2f}%")
    st.write(f"Precision: {selected_metrics['Precision']:.2f}")
    st.write(f"Recall: {selected_metrics['Recall']:.2f}")
    st.write(f"F1 Score: {selected_metrics['F1 Score']:.2f}")

    st.subheader('Confusion Matrix')
    st.image(f'Plots/confusion_matrix_{model_name}.png')

    if model_name in ['XGBoost', 'SVM']:
        st.subheader('ROC Curve')
        st.image(f'Plots/roc_curve.png')

        st.subheader('Precision-Recall Curve')
        st.image(f'Plots/precision_recall_curve.png')

    if model_name in ['XGBoost', 'Decision Tree']:
        st.subheader('Feature Importance')
        st.image(f'Plots/feature_importance_{model_name}.png')
    
def main():
    # Giving a title
    st.title('Heart Disease Prediction Web App')

    # Getting the input data from the user
    age = st.number_input('Age', min_value=0, max_value=120, step=1)
    sex = st.number_input('Sex (0 = Female, 1 = Male)', min_value=0, max_value=1, step=1)
    cp = st.number_input('Chest Pain Type (0-3)', min_value=0, max_value=3, step=1)
    trtbps = st.number_input('Resting Blood Pressure', min_value=0)
    chol = st.number_input('Cholesterol', min_value=0)
    fbs = st.number_input('Fasting Blood Sugar (1 = True, 0 = False)', min_value=0, max_value=1, step=1)
    restecg = st.number_input('Resting Electrocardiographic Results (0-2)', min_value=0, max_value=2, step=1)
    thalachh = st.number_input('Maximum Heart Rate Achieved', min_value=0)
    exng = st.number_input('Exercise Induced Angina (1 = Yes, 0 = No)', min_value=0, max_value=1, step=1)
    oldpeak = st.number_input('ST Depression Induced by Exercise', min_value=0.0, format="%.1f")
    slp = st.number_input('Slope of the Peak Exercise ST Segment (0-2)', min_value=0, max_value=2, step=1)
    caa = st.number_input('Number of Major Vessels (0-4)', min_value=0, max_value=4, step=1)
    thall = st.number_input('Thalassemia (1 = Normal, 2 = Fixed Defect, 3 = Reversable Defect)', min_value=1, max_value=3, step=1)

    input_data = [age, sex, cp, trtbps, chol, fbs, restecg, thalachh, exng, oldpeak, slp, caa, thall]

    # Code for Prediction
    diagnosis = ''

    # Creating buttons for Prediction
    if st.button('Heart Disease Test Using XGBoost Classifier with testing accuracy of 97.27'):
        diagnosis = predict_heart_disease(models['XGBoost'], input_data)

    if st.button('Heart Disease Test Using SVM Classifier with testing accuracy of 86.77'):
        diagnosis = predict_heart_disease(models['SVM'], input_data)

    if st.button('Heart Disease Test Using Decision Tree Classifier with testing accuracy of 97.66'):
        diagnosis = predict_heart_disease(models['Decision Tree'], input_data)

    st.success(diagnosis)

    # Adding a selectbox for model selection
    st.subheader('Select Model for Analytics')
    model_name = st.selectbox('Choose a model', list(models.keys()))

    # Adding a button for showing model analytics
    if st.button('Show Model Analytics'):
        show_model_analytics(model_name)

if __name__ == '__main__':
    main()