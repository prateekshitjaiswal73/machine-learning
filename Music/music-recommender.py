import pandas as pd
from sklearn.tree import DecisionTreeClassifier
import joblib

music = pd.read_csv("music.csv")
X = music.drop(columns=["genre"])
y = music["genre"]
model = DecisionTreeClassifier()
model.fit(X.values, y)
joblib.dump(model, 'music-recommender.joblib')
