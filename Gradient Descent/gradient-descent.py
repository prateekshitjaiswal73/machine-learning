import pandas as pd
import numpy as np
from sklearn.linear_model import LinearRegression

def gradient_descent(X, y):
    m_curr = b_curr = 0
    it = 10000
    n = len(X)
    learning_rate = 0.08
    for i in range(it):
        y_predic = m_curr * X + b_curr
        cost = (1/n) * sum([i ** 2 for i in (y - y_predic)])
        md = - (2 / n) * sum(X * (y - y_predic))
        bd = - (2 / n) * sum(y - y_predic)
        m_curr = m_curr - learning_rate * md
        b_curr = b_curr - learning_rate * bd
        print("m {}, b {}, cost {}, iter {}".format(m_curr, b_curr, cost, i))


X = np.array([1, 2, 3, 4, 5])
y = np.array([5, 7, 9, 11, 13])

gradient_descent(X, y)
